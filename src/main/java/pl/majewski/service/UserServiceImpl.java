package pl.majewski.service;

import pl.majewski.dao.UserDao;
import pl.majewski.dao.impl.UserDaoImpl;
import pl.majewski.model.User;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public UserServiceImpl(){
        this.userDao = new UserDaoImpl();
    }

    @Override
    public void save(User user) {
        userDao.openCurrentSessionWithTransaction();
        userDao.persist(user);
        userDao.closeCurrentSessionWithTransaction();
    }

    @Override
    public User getById(int id) {
        userDao.openCurrentSessionWithTransaction();
        User user = userDao.getById(id);
        userDao.closeCurrentSessionWithTransaction();

        return user;
    }

    @Override
    public List<User> getAll() {
        userDao.openCurrentSessionWithTransaction();
        List<User> users = userDao.getAll();
        userDao.closeCurrentSessionWithTransaction();

        return users;
    }

    @Override
    public void delete(int id) {
        userDao.openCurrentSessionWithTransaction();
        User user = userDao.getById(id);
        userDao.delete(user);
        userDao.closeCurrentSessionWithTransaction();
    }

    @Override
    public void update(User user) {
        userDao.openCurrentSessionWithTransaction();
        userDao.update(user);
        userDao.closeCurrentSessionWithTransaction();
    }
}
