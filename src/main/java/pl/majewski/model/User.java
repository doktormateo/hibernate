package pl.majewski.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;
    @Column
    private String name;
    @Column
    private String lastName;
    @Column
    private LocalDate insertTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfCreation() {
        return insertTime;
    }

    public void setDateOfCreation(LocalDate dateOfCreation) {
        this.insertTime = dateOfCreation;
    }
}
