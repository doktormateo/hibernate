package pl.majewski.dao;

import pl.majewski.model.User;

import java.util.List;

public interface UserDao extends SessionOperations{

    void persist(User user);
    User getById(int id);
    List<User> getAll();
    void delete(User user);
    void update(User user);
}
