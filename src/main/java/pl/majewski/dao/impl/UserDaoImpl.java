package pl.majewski.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.majewski.dao.UserDao;
import pl.majewski.model.User;

import java.util.List;

public class UserDaoImpl implements UserDao {
    private Session currentSession;
    private Transaction currentTransaction;

    public SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        return sessionFactory;
    }

    public Session getCurrentSession(){
        return currentSession;
    }

    @Override
    public Session openCurrentSession() {
        currentSession.getSessionFactory().openSession();
        return currentSession;
    }

    @Override
    public Session openCurrentSessionWithTransaction() {
       currentSession = getSessionFactory().openSession();
       currentTransaction = currentSession.beginTransaction();
       return currentSession;
    }

    @Override
    public void closeCurrentSession() {
        currentSession.getSessionFactory().close();
    }

    @Override
    public void closeCurrentSessionWithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    @Override
    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    @Override
    public void persist(User user) {
        getCurrentSession().persist(user);
    }

    @Override
    public User getById(int id) {
        return getCurrentSession().get(User.class, id);
    }

    @Override
    public List<User> getAll() {
        return getCurrentSession().createQuery("From User").list();
    }

    @Override
    public void delete(User user) {
        getCurrentSession().delete(user);
    }

    @Override
    public void update(User user) {
        getCurrentSession().update(user);
    }
}
