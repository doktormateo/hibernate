package pl.majewski.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public interface SessionOperations {
    Session openCurrentSession();
    Session openCurrentSessionWithTransaction();

    void closeCurrentSession();
    void closeCurrentSessionWithTransaction();

    SessionFactory getSessionFactory();
    Transaction getCurrentTransaction();
}
